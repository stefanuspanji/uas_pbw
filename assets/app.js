var selectedRow = null;
var btn = document.querySelector(".button")
btn.addEventListener("click", employdata);


function employdata(){
  var ax = read_Input_Value();
  if (selectedRow == null){
    create_Tr_Td(ax);
    remove_input_value()
  }
  else{   
    updatefunc(ax); 
    remove_input_value(); 
    selectedRow = null;
  }

}

function read_Input_Value(){
  var redemp={} 
  redemp["tanggal"]   = document.querySelector(".frmtanggal").value;
  redemp["waktu"]     = document.querySelector(".frmwaktu").value;
  redemp["kegiatan"]  = document.querySelector(".frmkegiatan").value;
  return redemp
}
function create_Tr_Td(x){
  var empTable = document.querySelector(".list");
  var emp_tr = empTable.insertRow(empTable.length);
  var emp_td1 = emp_tr.insertCell(0);
  var emp_td2 = emp_tr.insertCell(1);
  var emp_td3 = emp_tr.insertCell(2);
  var emp_td4 = emp_tr.insertCell(3);
  var emp_td5 = emp_tr.insertCell(4);
  var totalRowCount = document.querySelector(".list tr").length;
  emp_td1.innerHTML = empTable.rows.length-1;

    emp_td2.innerHTML = x.tanggal;   
    emp_td3.innerHTML = x.waktu;
    emp_td4.innerHTML = x.kegiatan;
  
  emp_td5.innerHTML = '<a class="edt" onClick="onEdit(this)">Edit</a>  / <a class="dlt" onClick="onDelete(this)">Delete</a>';
}

function remove_input_value(){
  document.querySelector(".frmtanggal") .value= " ";
  document.querySelector(".frmwaktu")   .value= " ";
  document.querySelector(".frmkegiatan").value= " ";  
}

function onEdit(y){
  console.log(y);
    selectedRow = y.parentElement.parentElement;
    document.querySelector(".frmtanggal") .value = selectedRow.cells[1].innerHTML;
    document.querySelector(".frmwaktu")   .value = selectedRow.cells[2].innerHTML;
    document.querySelector(".frmkegiatan").value = selectedRow.cells[3].innerHTML;
}

function updatefunc(redemp){
  selectedRow.cells[1].innerHTML = redemp.tanggal;
  selectedRow.cells[2].innerHTML = redemp.waktu;
  selectedRow.cells[3].innerHTML = redemp.kegiatan;

}

function onDelete() {
    if (confirm('Dihapus beneran ?')) {
        var selectdelete  = document.querySelector("a.dlt");
        selectdelete      = selectdelete.parentElement.parentElement.remove(0);
    }
}
